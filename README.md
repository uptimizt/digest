# Digest

https://github.com/uptimizt/digest


## интересные посты
- Лучшие сервисы для крауд маркетинга https://bizzapps.ru/b/rating-crowd-marketing/
- low code https://bizzapps.ru/b/low-code/
- Разница между программистами https://bizzapps.ru/b/raznitsa-mezhdu-programmistami-senior-middle-junior/
- Копирайтинг: искусство интересных текстов https://bizzapps.ru/b/copywriting/



## интересные сервисы и приложения для бизнеса
- Зум https://bizzapps.ru/p/zoom/
- Яндекс Почта https://bizzapps.ru/p/yandex-pochta/ 
- ВордСтат https://bizzapps.ru/p/wordstat-yandex/
- Викс конструктор сайтов https://bizzapps.ru/p/wix/

